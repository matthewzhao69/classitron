"""
This file contains Consumers (essentially Websocket endpoints)
for Django Channels.

Each Consumer will likely have a connect() function, a
disconnect() function, and a receive() function. To respond/
send data through the websocket, you can use consumer.send() (see
below for examples).

More information:
https://channels.readthedocs.io/en/latest/topics/consumers.html

You **CANNOT** access Django database models from an asynchronous consumer!
If you need to access a model, use database_sync_to_async and a separate function:
https://channels.readthedocs.io/en/latest/topics/databases.html
"""

from asgiref.sync import async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer
import json

class AnnotationConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        # Add consumer to "annotations" channel group
        # this basically means that any data sent from this
        # consumer goes out to all connected clients
        # Useful because we want to update all clients at the
        # same time for annotations
        print("a")
        await self.channel_layer.group_add(
            "annotations",
            self.channel_name
        )
        print("b")

        await self.accept()
        print("c")

    async def disconnect(self, close_code):
        # Leave the "annotations" channel group
        # so we don't leave the consumer floating inside the group
        # after the client is disconnected
        await self.channel_layer.group_discard(
            "annotations",
            self.channel_name
        )

    async def receive(self, text_data):
        # Send message to room group
        # NOTE: NOT THE SAME AS sending a message
        # to the websocket! This sends an event
        # to the other consumers and calls
        # the function named in type
        # (in this case, it calls annotation()).
        await self.channel_layer.group_send(
            "annotations",
            {
                'type': 'annotation',
                'message': text_data
            }
        )

    async def annotation(self, event):
        await self.send(text_data=event["message"])
