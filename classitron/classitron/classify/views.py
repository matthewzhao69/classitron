"""
This file contains views (basically URL paths) for classitron.

Because we're lazy, we're using DRF ModelViewSets, which abstracts out
all the REST API routing (/api/v2/images, /api/v2/images/1/, etc.)
All we need to do to make a ModelViewSet work is to give it a serializer
(see serializers.py); it will figure everything else out itself.

https://www.django-rest-framework.org/api-guide/viewsets/#modelviewset
"""

from django.contrib.auth.models import User, Group
from django.middleware import csrf
from django.views import View
from django.http import HttpResponse, JsonResponse
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from django_q.tasks import async_task
from classitron.classify.models import ODLC, Mission, Image
from classitron.classify.serializers import UserSerializer, GroupSerializer, ODLCSerializer, MissionSerializer, ImageSerializer

# TODO: Determine - do we need these User/Group ViewSets?
# It's nice to have API access to manage user and group but
# it doesn't seem necessary and may expose a larger attack surface
# while also cluttering code.
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint to view and edit users.

    Quick explanation: Viewsets basically handle all the usual
    boilerplate around interacting with models, meaning we
    don't have to write individual views for viewing users,
    editing users, etc.
    """
    queryset = User.objects.all().order_by("-date_joined")
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAdminUser]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint to view and edit groups.
    """
    queryset = Group.objects.all().order_by("-date_joined")
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAdminUser]


class ODLCViewSet(viewsets.ModelViewSet):
    """
    API endpoint to view and edit ODLCs.
    """
    queryset = ODLC.objects.all()
    serializer_class = ODLCSerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=["POST"], permission_classes=[permissions.IsAuthenticated])
    def upload_to_interop(self, request, pk=None):
        """
        API endpoint that uploads the specified ODLC to the Interop server.

        NOTE: This method only contains a bit of boilerplate. The actual logic will happen
        in the model.
        """
        print("yeet")
        async_task("classitron.classify.tasks.upload_to_interop", self.queryset.get(pk=pk))

        return Response(200)


class MissionViewSet(viewsets.ModelViewSet):
    """
    API endpoint to view and edit Missions.
    """
    queryset = Mission.objects.all()
    serializer_class = MissionSerializer
    permission_classes = [permissions.IsAuthenticated]


class ImageViewSet(viewsets.ModelViewSet):
    """
    API endpoint to view and edit Missions.
    """
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=["GET"], permission_classes=[permissions.IsAuthenticated])
    def odlcs(self, request, pk=None):
        """
        Get all ODLC objects associated with the specified Image.
        """
        urmom = ODLCSerializer(self.queryset.get(pk=pk).odlc_set.all(), many=True, context={"request": request}).data
        print(urmom)
        return JsonResponse(urmom, safe=False)
