"""
This routing file serves to point to websocket endpoints
within the classify app.

This file is included within the global classitron route config.
"""
from django.urls import path
from classitron.classify import consumers

websocket_urlpatterns = [
    path(r'ws/annotations/', consumers.AnnotationConsumer),
]
