"""
This file contains model classes (Django ORM) for Classitron.

These model classes abstract out database and file storage for us.
We can simply fetch an object (e.g. in a Viewset, self.queryset.get(id))
and use it like a regular Python object:

odlc = self.queryset.get(0)
print(odlc.is_emergent) # Prints the value of the is_emergent field
odlc.is_emergent = True # Sets is_emergent to true

To save changes to the object to the database, use .save():
odlc.save()

We can also use models in ModelSerializers as a convenient and easy way
of formatting and checking API objects (see serializers.py).
- NOTE: to mark a model field as optional, put blank=True.
"""

from django.db import models
from django.contrib import admin
from interoperator.odlc import Color, Shape, Orientation, ODLCStandard, ODLCEmergent
import math

class Mission(models.Model):
    """
    A model representing a Mission (which contains ODLCs).
    """
    # Store interop mission ID
    mission_id = models.IntegerField()

    # Add on a description variable
    description = models.TextField(blank=True, default="")


class Image(models.Model):
    """
    A model representing an Image taken by the drone.
    """
    mission = models.ForeignKey(Mission, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="images/")
    latitude = models.FloatField()
    longitude = models.FloatField()
    height = models.FloatField()


class ODLC(models.Model):
    """
    An model representing an ODLC with attributes
    (x, y, width, height, and metadata).

    This will be serialized by a serializer
    class later, so we can convert directly
    from JSON to a model object.
    """

    # Common, required atributes
    interop_id = models.IntegerField(blank=True, default=-1) # Interop server given ID, not same as reg. ID
    parent_image = models.ForeignKey(Image, on_delete=models.CASCADE) # Image this is linked to
    is_emergent = models.BooleanField() # whether the ODLC object is emergent or standard
    autonomous = models.BooleanField() # whether the ODLC object was classified without human intervention
    image = models.ImageField(blank=True) # Cropped ODLC image, will be added by Django-Q task
    status = models.IntegerField(blank=True, default=0) # 0 = unreviewed, 1 = needs peer review, 2 = done

    latitude = models.FloatField(blank=True, default=-1)
    longitude = models.FloatField(blank=True, default=-1)

    # Crop data ((x, y): left corner, width and height are width and height)
    x = models.IntegerField()
    y = models.IntegerField()
    width = models.IntegerField()
    height = models.IntegerField()

    # Create all of the additional standard ODLC fields (these will be added on top of ODLCMixin's fields)
    # the choices=[(orientation, orientation.value) for orientation in Orientation] is a handy
    # Python shorthand for limiting our models to just the values inside our Orientation enum.
    # (for example, now the orientation field will only accept valid Orientations according to the
    # Interoperator enum.)
    orientation = models.CharField(choices=[(orientation.value, orientation.value) for orientation in Orientation],
                                   max_length=32,
                                   blank=True,
                                   default="")

    # We only need 1 char for alphanumeric
    # Again, same thing with the enum
    alphanumeric = models.CharField(max_length=1, blank=True, default="")
    alphanumeric_color = models.CharField(choices=[(color.value, color.value) for color in Color],
                                          max_length=32,
                                          blank=True,
                                          default="")

    shape = models.CharField(choices=[(shape.value, shape.value) for shape in Shape],
                             max_length=32,
                             blank=True,
                             default="")
    shape_color = models.CharField(choices=[(color.value, color.value) for color in Color],
                                   max_length=32,
                                   blank=True,
                                   default="")

    # Add emergent attributes
    # Description is added on top of ODlCMixin's models
    description = models.TextField(blank=True, default="")

    def convert_to_lat_lon(self, x: int, y: int, image_width: int, image_height: int, pose: tuple):
        """
        Convert a pixel (x, y) into latitude and longitude.

        :param x: x coordinate of the pixel
        :param y: y coordinate of the pixel
        :param pose: tuple in format (camera_rotation_x_axis, camera_rotation_y_axis, image_lat, image_lon, height)
        :return: tuple in format (latitude, longitude)
        """
        # Naive pixel to lat/lon calculation
        # Assumes camera faces straight down and horizontal axis aligns perfectly west-east
        horizontal_fov = 2 * math.degrees(math.atan(36 / (35 * 2 * 1.6)))
        vertical_fov = 2 * math.degrees(math.atan(24 / (35 * 2 * 1.6)))
        print(horizontal_fov, vertical_fov)

        horizontal_size = 100 / math.tan(math.radians(90-(horizontal_fov/2))) * 2
        vertical_size = 100 / math.tan(math.radians(90-(vertical_fov/2))) * 2
        print(horizontal_size, vertical_size)

        pixel_size_h = horizontal_size / 1920 / 364000
        pixel_size_v = horizontal_size / 1080 / 288200

        print(pixel_size_h, pixel_size_v)

        midpoint_w = image_width // 2
        midpoint_h = image_height // 2

        lat = pose[2] + pixel_size_h * (x - midpoint_w)
        lon = pose[3] + pixel_size_v * (y - midpoint_h)

        return lat, lon

    def save(self, *args, **kwargs):
        """
        Custom save() method
        tags the ODLC with latitude and longitude on save.
        TODO: There's probably a better way to do this (async task?)
        """
        # Tag with latitude, longitude
        lat, lon = self.convert_to_lat_lon(self.width/2 + self.x,
                                           self.height/2 + self.y,
                                           self.parent_image.image.width,
                                           self.parent_image.image.height,
                                           (0,
                                            0,
                                            self.parent_image.latitude,
                                            self.parent_image.longitude,
                                            self.parent_image.height))
        self.latitude = lat
        self.longitude = lon
        super(ODLC, self).save(*args, **kwargs)

    def as_interoperator_odlc(self):
        """
        Convert ODLC to Interoperator ODLC (see Interoperator).
        """
        if self.is_emergent:
            # Return an emergent ODLC
            return ODLCEmergent(self.parent_image.mission.id,
                                self.latitude,
                                self.longitude,
                                self.description,
                                self.interop_id)
        else:
            return ODLCStandard(self.parent_image.mission.id,
                                self.latitude,
                                self.longitude,
                                Orientation(self.orientation),
                                Shape(self.shape),
                                Color(self.shape_color),
                                self.alphanumeric,
                                Color(self.alphanumeric_color),
                                False,
                                self.interop_id)
