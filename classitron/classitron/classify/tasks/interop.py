"""
Contains interop asynchronous tasks for DjangoQ to run.

Currently, the ODLC ViewSet is configured with an action method to upload
to interop (see views.py). In normal human speak, this means that a POST to
/api/v2/odlcs/{id}/upload_to_interop/ will call the upload_to_interop() function
in this file, uploading the ODLC to the interop server. More accurately, it queues
the function to be run by one of the DjangoQ workers, but either way it more or less
works the same.

https://django-q.readthedocs.io/en/latest/tasks.html
"""
from interoperator.connection import InteropConnection, InteropException
from interoperator.odlc import ODLC as InteropODLC
from classitron.classify.models import ODLC
from asgiref.sync import sync_to_async, async_to_sync
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
from PIL import Image
from io import BytesIO
import asyncio
import math



def upload_to_interop(odlc: ODLC):
    """
    Upload an ODLC object to an Interop Server.
    This function wraps an asynchronous __upload_to_interop
    function - this is necessary due to how Django and
    Django-Q are set up - full Django async support should
    come soon
    """
    print(odlc.as_interoperator_odlc())
    # Upload metadata and get interop ID
    odlc_id = async_to_sync(upload_interop_metadata)(odlc.as_interoperator_odlc())
    odlc.interop_id = odlc_id


    # Use Interop ID to upload a cropped image
    odlc.parent_image.image.open()
    odlc_image = async_to_sync(add_image_to_odlc)(odlc_id,
                                                  [odlc.x, odlc.y, odlc.width, odlc.height],
                                                  odlc.as_interoperator_odlc(),
                                                  Image.open(odlc.parent_image.image))

    if settings.DEBUG:
        # Save cropped image to a StringIO buffer, then
        # Use Django's InMemoryUploadedFile to save it to the model
        # This avoids writing to disk
        # NOTE: We ONLY save to the odlc.image field for debug!
        # During the actual mission, we should not need to keep a
        # copy of the uploaded image in memory.
        odlc.image = InMemoryUploadedFile(odlc_image, None, 'foo.jpg', 'image/png',
                                          odlc_image.tell(), None)

    # Save the ODLC
    odlc.save()


async def upload_interop_metadata(odlc: InteropODLC):
    """
    Upload an ODLC object to an interop server.
    """
    print("runnin")
    interop_conf = settings.INTEROP

    interop_url_formatted = f"{interop_conf['URL']}:{interop_conf['PORT']}"
    async with InteropConnection(interop_url_formatted) as conn:
        sessionid = await conn.login(interop_conf['USER'], interop_conf['PASS'])
        print("UR MOOOOOOOOOOOOOOOOOOOOOOOOOOM", sessionid)

    cookies = {"sessionid": sessionid}
    async with InteropConnection(interop_url_formatted, cookies=cookies) as conn:
        print(odlc)
        if odlc.id == -1:
            # ODLC is new, POST it and store ID
            try:
                odlc = await conn.post_odlc(odlc)
            except InteropException as e:
                print(e.detail)
            odlc_id = odlc.id
            return odlc_id
        else:
            # ODLC was already uploaded, UPDATE (PUT) it
            print(odlc.id)
            await conn.update_odlc(odlc)
            return odlc.id


async def add_image_to_odlc(odlc_id: int, crop_box: list, odlc: InteropODLC, parent_image: Image):
    """
    Crop ODLC image and add it to the ODLC.

    :param: x
    :return: BytesIO buffer containing the image as bytes.
    """
    # Convert from sensible crop box to PIL friendly version
    # i.e. (x, y, width, height) -> (x, y, right, lower)
    crop_box[2] += crop_box[0]
    crop_box[3] += crop_box[1]

    # Crop image
    cropped_image = parent_image.crop(tuple(crop_box))
    cropped_image_buffer = BytesIO()
    cropped_image.save(cropped_image_buffer, format="PNG")

    # Upload to interop
    interop_conf = settings.INTEROP

    interop_url_formatted = f"{interop_conf['URL']}:{interop_conf['PORT']}"
    async with InteropConnection(interop_url_formatted) as conn:
        sessionid = await conn.login(interop_conf['USER'], interop_conf['PASS'])

    cookies = {"sessionid": sessionid}
    async with InteropConnection(interop_url_formatted, cookies=cookies) as conn:
        await conn.post_odlc_image(odlc.id, cropped_image_buffer.getvalue())

    return cropped_image_buffer

