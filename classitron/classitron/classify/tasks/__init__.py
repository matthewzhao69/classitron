"""
Contains async tasks for DjangoQ to run.

https://django-q.readthedocs.io/en/latest/tasks.html
"""

from classitron.classify.tasks.interop import *
