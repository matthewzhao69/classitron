"""
Serializers for the Classify API.
These essentially convert back and forth between models, querysets etc.
and native Python datatypes and formats such as JSON, XML etc.

https://www.django-rest-framework.org/api-guide/serializers/
"""

from django.contrib.auth.models import User, Group
from rest_framework import serializers
from classitron.classify.models import ODLC, Mission, Image


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialize the User model.

    Essentially what we're doing here with the Meta nested class
    is telling djangorestframework that we want to be able to
    serialize the User model (from django.auth), keeping the url,
    username, email and groups fields.
    """
    class Meta:
        model = User
        fields = ["url", "username", "email", "groups"]


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialize the Group model.

    We're basically doing the same thing as in UserSerializer but
    with the Group model and url + name fields.
    """
    class Meta:
        model = Group
        fields = ["url", "name"]


class ODLCSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialize the ODLC model.

    Setting the fields attribute to __all__ tells DRF that we
    want to just serialize all the fields on the model.
    """
    class Meta:
        model = ODLC
        fields = "__all__"


class MissionSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialize the Mission model.
    """
    class Meta:
        model = Mission
        fields = "__all__"


class ImageSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialize the Image model.
    """
    class Meta:
        model = Image
        fields = "__all__"
