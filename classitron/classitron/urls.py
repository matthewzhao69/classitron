"""
Backend URLs.
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import routers
from classitron.classify import views

# Register our viewsets' sub-URLs with a router
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'odlcs', views.ODLCViewSet)
router.register(r'missions', views.MissionViewSet)
router.register(r'images', views.ImageViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', RedirectView.as_view(url='/api/v2/', permanent=True)),
    path('prometheus/', include('django_prometheus.urls')),
    path('api/v2/', include(router.urls)),
    path('api/v2/auth/', include('rest_auth.urls')),
    path('admin/', admin.site.urls)
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
