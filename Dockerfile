FROM python:latest
COPY . /classitron
WORKDIR /classitron

RUN pip3 install -r requirements.txt
RUN NEW_UUID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 256 | head -n 1)
RUN sed -i "s+localhost+db+g" ./classitron/classitron/settings.py

RUN mkdir -p ~/.local/share/classitron
RUN echo -n $NEW_UUID > ~/.local/share/classitron/password.txt
RUN cp ./classitron/db.conf ~/.local/share/classitron

WORKDIR classitron/
RUN pip3 install gunicorn
CMD gunicorn --workers=10 --bind 0.0.0.0:3000 classitron.wsgi
