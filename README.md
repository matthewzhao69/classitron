# Classitron: A distributed manual classification server. #
Classitron is AmadorUAVs' purpose built system for manually classifying
objects in an image. Usage-wise, it is similar to a traditional
annotation application; in reality, it's anything but.

## About The Application ##
In simple terms, Classitron is a webpage you can sign into to
annotate images. It can support multiple people on the system
classifying and annotating images in real time.

In more technical terms, Classitron is a local Django webserver
which uses a MySQL backend to provide seamless multi-user
support. Classitron will provide new images to classify as
they arrive, and will store or pass on the annotations as
soon as they are completed.

## How To Use Classitron ##
1. Sign into the webpage with your AmadorUAVs credentials.
(Users can be added/modified via the Django Admin board.)
2. Draw a bounding box around each object and label them.
3. Hit submit.
4. Wait for the next image.

## Setting up a Development Environment ##
To set up a local environment for developing and testing:
1. Run `./setup.sh`. This should set up everything you need.
2. Activate the virtual environment with `source venv/bin/activate`. You should see a `(venv)` in your shell prompt.
4. Set up Django MySQL tables with `python3 manage.py makemigrations classify && python manage.py migrate`
5. `python3 manage.py runserver` to start up the development server.

### Notes for Devs ###
- Classitron is currently running on a MongoDB backend via `django-rest-framework-mongoengine`. If you experience issues with the MongoDB backend, you can simply switch Classitron to MySQL by changing out the imports and modifying `settings.py`. We recommend you file an issue if you experience any issues.
- Classitron runs on Django and the Django REST framework (DRF). While the code should be fairly high-level and understandable, it will probably help to know how these frameworks work. If you run into any bugs with Django or DRF itself, please report those bugs to their respective maintainers.
