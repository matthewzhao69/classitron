#!/bin/bash
# Script to set up Classitron Django app securely,
# including mysql database.

# Find package manager + install necessary
shopt -s nocasematch

if [ -f /usr/bin/apt ]; then
	sudo apt update && sudo apt upgrade
	sudo apt install python3-pip python3-mysqldb python3-dev default-libmysqlclient-dev mysql-client python3-venv mysql-server
	pip3 install --user mysqlclient

	# enable mysqld
	echo "Enabling MySQL daemon..."
	echo "WARNING WORKS ON SYSTEMD SYSTEMS ONLY SORRY GENTOO"
	sudo systemctl enable mysql

	echo "Starting MySQL daemon..."
	sudo systemctl start mysql
elif [ -f /usr/bin/pacman ]; then
	sudo pacman -Syu
	sudo pacman -S mysql python-pip python-mysqlclient

	# enable mysqld
	echo "Enabling MySQL daemon..."
	sudo systemctl enable mysqld

	echo "Entering MySQL setup..."
	sudo mysql_install_db --basedir=/usr/ --ldata=/var/lib/mysql/
	sudo chown -R mysql:mysql /var/lib/mysql

	echo "Starting MySQL daemon..."
	sudo systemctl start mysqld
else
	echo "Sorry, looks like your system/package manager's not supported :("
	echo "If you have a Ubuntu/Debian system, consider installing aptitude."
	echo
	printf "Autodep install failed. Continue anyway? (Y/n): "
	read reply
	if [[ "$reply" != "y" ]]; then
		exit 1
	fi
fi

# Activate venv
python3 -m venv venv/
source venv/bin/activate

# install deps
echo "Installing deps..."
pip3 install -r requirements.txt

# Install interop client
cd extern/auvsisuas-client-stripped
pip3 install -r requirements.txt
python3 setup.py install

# Generate a random 32 character string for db password and store it in user's bashrc
echo "Generating db access credentials. This stores a random 256 character string in ~/.classitron."
echo "These credentials ONLY work on your local system. Never share them."
NEW_UUID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 256 | head -n 1)

echo -n $NEW_UUID > ~/.classitron

# Protect permissions
chmod a-r ~/.classitron
chmod a-w ~/.classitron
chmod u+r ~/.classitron
chmod u+w ~/.classitron

# Create classitron mysql user/pass
echo "Creating classitron MySQL user and database."
sudo mysql -u root -e "DROP USER IF EXISTS classitron;"
sudo mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'classitron'@'localhost' IDENTIFIED BY '$NEW_UUID';"
sudo mysql -u root -e "CREATE DATABASE IF NOT EXISTS classitron;"

echo "Done setting up."
echo "-----IMPORTANT------"
echo "Before running the project, you must run:"
echo "	source venv/bin/activate"
echo ""
echo "Before first run or after you make changes to models, please run:"
echo "	python3 manage.py makemigrations classify"
echo "	python3 manage.py migrate"
